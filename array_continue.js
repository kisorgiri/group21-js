var bikes = [{
    name: 'xr',
    color: 'red',
    cc: 190,
    price: 1234,
    company: 'honda'
}, {
    name: 'CBR',
    color: 'orange',
    cc: 1000,
    price: 122,
    company: 'honda'
}, {
    name: 'pulsar',
    color: 'black',
    cc: 200,
    price: 12,
    company: 'bajaj'

}, {
    name: 'pulsar',
    color: 'black',
    cc: 200,
    price: 12,
    company: 'bajaj'

}, {
    name: 'duke',
    color: 'black',
    cc: 200,
    price: 12,
    company: 'ktm',
}, {
    name: 'fz',
    color: 'black',
    cc: 200,
    price: 12,
    company: 'yamaha'
}, {
    name: 'vr',
    color: 'black',
    cc: 200,
    price: 12,
    company: 'hartford'
}]

bikes.forEach(function(item, i) {
    item.status = 'available';
})

// array forEach
// filter

var blackBikes = bikes.filter(function(item, i) {
    if (item.color === 'black') {
        return true;
    }
});
console.log('blackBikes >>', blackBikes);
var ktmBikes = blackBikes.filter(function(item, i) {
    if (item.company === 'ktm') {
        return item;
    }
})
console.log('ktm bikes >>', ktmBikes);
bikes.map(function(item, i) {
    if (item.color === 'black' && item.company === 'ktm') {
        item.status = 'sold';
    }
})
console.log('bikes above splice >>', bikes.length);


bikes.forEach(function(item, i) {
    if (item.status === 'sold') {
        bikes.splice(i, 1);
    }
})
console.log('bikes >>', bikes.length);

// every==> return BOOLEAN
// some ==>  return Boolean
// find ==> return 1st element that match the condition

// reduce
var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
numbers.reduce(function(acc, item, index, sourceArray) {
    console.log('acc >>', acc);
    var sum = acc + item;
    return 'sum';
}, 0)