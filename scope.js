// // scope is visibility accessibility of application data(var,const functions)
// // type of scope
// // 1 global scope
// // 2 local scope
// // 3 block scope

// // global scope

// // all allocation (declarition) declared in outermost layer of a JS file is global scope

// // function welcome(){


// // }

// // var hi = 'hi';

// // var test = 'sdlfjl';

// // global scope are accsible within a file

// // 2.functional scope (local scope)
// // all declarition within a function block
// // var hi = 'hello'; // global scope
// function goodBye() {
//     // global scope
//     "use strict";

//     hi = 'see you again';
//     //abc is local(functional scope)
//     console.log('hi >>', hi);
//     // console.log('abc >>', abc);
// }

// function sayHello(name) {
// 	"use strict";

//     var abc = 'abcd';
//     // name is functional scope
//     console.log('abc >>',abc);
// }

// goodBye();
// console.log('abc outside >>>', hi);

var text = 'hi';

function welcome(name) {
    // let message = 'welcome';
    if (name) {
        let message = 'Hi ' + name + ' welcome to Brodway';
        console.log('message inside if >>', message);
    } else {
        let message = "welcome to tinkune";
    }
    console.log('message >>', message);

    function test() {

    }

    function hi() {

    }

}
welcome('abc');