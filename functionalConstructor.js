// functional constructor is basic building block for prototype based oop
// from ES6  we can write class in JS

// functional constructor is called using new keyword
// it should not return

// In OOP class/function constructor is group of user(developer) defined data type
console.log('this >>', this);

function Fruits() {
    // functional this (object) is initilization step for fucntional constructor
    console.log('this inside >>', this);

}
Fruits.prototype.origin = 'forest';
Fruits.prototype.color = 'red';
Fruits.prototype.getColor = function() {
    return this.color;
}
Fruits.prototype.setColor = function(newVal) {
    this.color = newVal;
    return this.color;
}

var apple = new Fruits(); // constructor call
console.log('apple >>', apple.setColor('blue'));

var arr = new Array();
// arr.
var obj = new Object();
// 
var str = new String();
