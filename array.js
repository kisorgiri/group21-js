// console.log('check length >>', fruits.length);
// console.log('find index >>', fruits.indexOf('banana')); // first occurance
// console.log('find last Index >>',fruits.lastIndexOf('banana'));

// additing elements to array
// 1 at first 
// fruits.unshift('orange'); // lenght
// // 2 at last
// var a = fruits.push('mango'); //length
// console.log('a is >>',a);
// // 3 at somewhere in between

// console.log('fruits now >>', fruits);


// removeing elements to array
// 1 at first 
// fruits[0];
// var firstItem = fruits.shift(); /// return first item
// console.log('fruits >>',fruits);
// console.log('first Item >>',fruits[0]);
// 2 at last
// var lastItem = fruits.pop();
// console.log('lste item >>',lastItem);
// console.log('fruits',fruits[fruits.length-1]);

// splice 
// splice is powerful methods that is used to add and remove items from array
// splice takes multiple arguments
// 1 argument is index
// 2 argument is number of items to be removed
// 3 rest arguments are items to be added
// 3 at somewhere in between
// fruits.splice(fruits.indexOf('kiwi'),2,'pineapple','watermelon');
// console.log('fruits after splice >>',fruits);

// console.log('to string >>',fruits.join('nepal'));

// loop
// repetation of action till certain condiition is matched

// forEach
// filter
// map
// reduce,
// some
// every
// find

var fruits = ['apple', 'kiwi', 'grape', 'mango', 'banana', 'apple', 'kiwi', 'grape', 'mango', 'banana'];


// fruits.forEach(function(item,i){
// 	console.log('item is >>',item);
// 	console.log('index is >>',i);
// })

// task 1
// prepare a function to extract unique array from given fruits array

// task 2
// prepare a function that will give a result of count fo repetative item from array
// expected output
// {
//     apple: 4,
//     kiwi: 1,
//     banana: 5
// }

// task 3
var students = [{
        name: 'ram',
        class: 2,
        roll: 22,
        house: 'red'
    }, {
        name: 'shyam',
        class: 1,
        roll: 22,
        house: 'green'
    }, {
        name: 'ram',
        class: 2,
        roll: 22,
        house: 'red'
    }, {
        name: 'ram',
        class: 1,
        roll: 22,
        house: 'green'
    }

]

// prepare a function that will group the data using given propertyName

// function groupBy(arr, propertyName) {

// }
// var result = groupBy(students, 'class')
//     // expected output
//     {
//         1: [{
//                 name: 'shyam',
//                 class: 1,
//                 roll: 22,
//                 house: 'green'
//             }, {
//                 name: 'ram',
//                 class: 1,
//                 roll: 22,
//                 house: 'green'
//             }],
//         2: [{
//             name: 'ram',
//             class: 2,
//             roll: 22,
//             house: 'red'
//         }, {
//             name: 'ram',
//             class: 2,
//             roll: 22,
//             house: 'red'
//         }]
//     }

//     expected output for house 
//      {
//         red: [{
//                 name: 'shyam',
//                 class: 1,
//                 roll: 22,
//                 house: 'green'
//             }, {
//                 name: 'ram',
//                 class: 1,
//                 roll: 22,
//                 house: 'green'
//             }],
//         green: [{
//             name: 'ram',
//             class: 2,
//             roll: 22,
//             house: 'red'
//         }, {
//             name: 'ram',
//             class: 2,
//             roll: 22,
//             house: 'red'
//         }]
//     }
