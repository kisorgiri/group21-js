//###########Task Part #########
function askMoney(amt) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('done');
        }, 2000);
    })

}

function buyPhone(model) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('phone');
        }, 1000);
    })
}

function takePhoto() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject('no photo');
        }, 1000);
    })
}

function editPhoto(photo) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('edited photo');
        }, 1000);
    })

}
//###########Task Part #########


//##############Execution Part############
console.log('i want to take photos');
console.log('ask money to buy phone');
askMoney(222)
    .then(function(data) {
        console.log('i have money');
        return buyPhone('s10')
    })
    .then(function(data) {
        console.log('resul tof buyPhone');
        return takePhoto();
    })
    .then(function(data) {
        console.log('result of takePhoto');
        return editPhoto();
    })
    .then(function(data) {
        console.log('result of edit photo');
    })
    .catch(function(err) {
        console.log('error in promise >>', err);
    })

//##############Execution Part############