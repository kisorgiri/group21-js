// //task part
// function buyCycle(model, ram) {
//     console.log('i am at cycle shop');
//     console.log('shopkeeper told me that my desired model will be available at evening');
//     setTimeout(function() {
//         console.log('cycle arrived at shop');
//         ram();
//     }, 2000);
// }
// // execution part
// buyCycle('giant',function() {
//     console.log('i have cycle now');
//     console.log('go for ride')
// });
// //task must be grouped with blocking and non-blocking
// // blocking task should be done once result is handled
// // non blocking will run without waiting result
// console.log('eat food');



function giveIELTS(location, cb) {
    // delay
    setTimeout(function(){
        cb(null,'done');
    },2000);
}

giveIELTS('bkt', function(err, done) {
    // actual callback block
    if (err) {
        console.log('error in callback', err);
    } else {
        console.log('success in callback', done);
    }
})


// truthy value and falsy value
// falsy value = 0,'',false, NaN, undefined, null