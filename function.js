// function is reusuable block of code that performs specific task
// function is basic building block for JS programming
// syntax
// function(){
// block of function

// }

// two ways of writing a function
// 1.expression syntax
// 2. declrative syntax

// 1.expression syntax
// JAVASCRIPT is loosey typed programming langugae
var a;
a = '22';
a = 33;
a = true;
// console.log('check somethign>>>',somethign);
// welcome();
// var welcome;


// var welcome = function() {

//     console.log('i am expression syntax function');
// }
// welcome();


// declrative syntax of writing function
sayHello();

function sayHello() {
    console.log('i am sayHello');
    console.log('i am declrative syntax function');
}

// welcome()// way of calling a function
// Hoisting ==> Hositing is mechanism that moves all the declration at top before execution


// types of function
// 1 named function
// 2 unnamed function (anynamous function)
// 3 function with argument
// 4 function with return type
// 5 IIFE (immidiately invoked functional expression)

// function hi(){

// }
// hi();
// var a = function(){

// }
// a();

// function (){

// }


function welcome(name, addr) { // name is allocation (placeholder)
    // console.log('what comes in <>>>>', name);
    var msg = 'Hi ' + name + ', welcome to ' + addr;
    console.log('msg >>', msg);
}

function sendMail(details) {
    console.log('details >>', details);
    console.log('from >>', details.from);

}
var data = {
    from: 'ram',
    to: 'shyam',
    cc: '@jklfd.com',
    message: 'registration succes',
    sub: 'hi',

}
sendMail(data);

// recommend to reduce more then 3 arguments into arguments

sendMail('33', 'd', null)

// welcome('ram');
// welcome('shyam', 'tinkune');
// welcome('hari', 'pokhara');
// welcome(null, 'lumbini');

// function with return type

// function addition(num1, num2) {
//     var result = num1 + num2;
//     return result;
// }

// var res = addition(2, 4);
// console.log('res is >>', res);

function goToKitchen() {
    var fruits = ['apple', 'orange'];
    var vegitables = ['potato'];
    console.log('i am go goToKitchen')
        // JAVASCRIPT is loosely typed programming language
        // es6 object shorthand
        // object destruct


    return {
        fruits: fruits,
        vegitables,
        ram: 'sdjkhf',
        a: 'dfd'
    };
    // return vanda tala code execute hudaina
    // console.log('welcome to somewhere');
    // return vegitables;
}
var {
    fruits
} = goToKitchen();
console.log('result >>', fruits);

// iiFE
(function() {
    console.log('i am anynamous function')
    console.log('I take help from IIFE to get exected')
})();