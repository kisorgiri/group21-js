// // callback is a function is passed as an argument to another function
// // it is a mechanism to handle result of async call

// function askMoney(amt, cbErr, cbSuccess) {
// 	//
//     setTimeout(function() { //delay in javascript
//     	cbSuccess();
//     	cbErr();
//     }, 1000);
// }

// function failure(err) {
//     console.log('err ins callback', err);
// }

// function success(data) {
//     console.log('cbSuccess in callback', data);
// }
// askMoney(3333, failure, success)
// console.log(' i will not wait result of askMoney');
// console.log('eat food');

// promise is also mechanism to handle resul of async call
// promise is an object
// object is non primitive data type
// which is collection of key value pair
// eg.
// var remote = {
// 	name:'epson',
// 	color:'white',
// 	price:222,
// 	state:'working',
// 	abcd:function(){

// 	}
// }
// promise is an object which holds result value of future
// promise has 3 different state
// 1. pending
// 2. onFullfilled
// 3. onRejection
// 4. settled
// once promise is settled promsie will not change its state

// promise has three methods
// then ==> then is used to handle both success and failure (recommend to use for success only)
// catch ==> catch is used to handle failure only
// finally ==> once promise is settled finally will be executed

// syntax
var abc = new Promise(function(success, failure) {
    //delay 
    setTimeout(function() {
        failure('bishal2222');

        success('bishal');

    }, 1000);
})
console.log('abc >>', abc);

abc
    .then(function(data) {
        console.log('succes in promise >>', data);
    })
    .catch(function(err) {
        console.log('error in catch bloack >>', err);
    })
    .finally(function() {
        console.log('promise is settled');
    })
console.log('non blocking task');

function askMoney(amt) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject();
        }, 2000);
    })
}

askMoney(333)
    .then(function(data) {
        console.log('success block >>', data);
    })
    .catch(function(err) {
        console.log('failure block >>', err);
    })
    .finally(function(val) {
        console.log('vfinally >>', val);
    })