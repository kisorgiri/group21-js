// all function (methods) that exist within JS is inbuilt methods
// setTimeout()
// typeof  check data type

// String
var str = "Broadway Infosys Nepal";
// console.log('length >>', str.length);
// console.log('to upperCase >>', str.toUpperCase());
// console.log('to lower case >>', str.toLowerCase());
// var trimmedStr = str.trim(); // remove white spaces
// console.log('removed space', trimmedStr.length);

console.log('sub string >>>', str.substr(0, 3));
console.log('sub string >>>', str.substring(0, 3));

var email = 'adlkjf@gmail.com';
console.log('check existance', email.includes('.com'));

// type conversion
var hobbies = 'singing,dancing,coding';
// console.log('to array >>',hobbies.split(''))

// task 1
// prepare a function that takes arguments in string as bellow
// 23m32s, 2m2s, 22m2s 2m22s
// result must be in object
// expected output
// var result  = calcualte('23m32s');
// result is expected as { minute:23,sec:32}

// Number
var num = 233;
console.log('fixed ', num.toFixed(2))
console.log('int only >>', parseInt(num));
// console.log('float >>',parseFloat(num));

function add(num1, num2) {
    console.log('type of >>', typeof(num1));
    return num1 * num2;
}
var result = add('22hh', 22);
console.log('result >>', result);
console.log('is NAN', isNaN(222));

console.log('type conversion >>', typeof(Number('2333')));

// boolean
// ! if true evaluates to false and vice versa
// truthy and falsy
// if(condition){

// }
// falsy value ==> 0, '', false, NaN, null,undefined

// Null,
// undefined


// Array and Object
// object

// object is collection of key value pair
// var obj = {
//     name: 'Broadway',
//     address: 'tinkune',
//     phone: 2222,
//     email: 'test@gmail.com'
// }

// console.log('check property >>', obj.hasOwnProperty('addresss'));

// function sendMail(details) {
//     if ('phone' in details) {
//         console.log("property exists");
//     } else {
//         console.log('property doesnot exists');
//     }
// }
// sendMail(obj);
// console.log('keys only >>', Object.keys(obj));
// console.log('values only >>', Object.values(obj));
// // string representation of object(Serialization)

// var stringRep = JSON.stringify(obj);
// console.log('original >>', obj);
// console.log('strigify >>', stringRep);
// console.log('back to original >>',JSON.parse(stringRep));

//loop (iteration)
// repetation of action till certain condition is matched
//for in 
var obj = {
    name: 'Broadway',
    address: 'tinkune',
    phone: 2222,
    email: 'test@gmail.com'
}

for (let ram in obj) {
    console.log('key >>', ram);
    console.log('value >>',obj[ram])
}